package com.yabushan.form.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.form.mapper.AutoUserTablesMapper;
import com.yabushan.form.domain.AutoUserTables;
import com.yabushan.form.service.IAutoUserTablesService;

/**
 * 用户建等级Service业务层处理
 *
 * @author yabushan
 * @date 2021-08-06
 */
@Service
public class AutoUserTablesServiceImpl implements IAutoUserTablesService
{
    @Autowired
    private AutoUserTablesMapper autoUserTablesMapper;

    /**
     * 查询用户建等级
     *
     * @param tId 用户建等级ID
     * @return 用户建等级
     */
    @Override
    public AutoUserTables selectAutoUserTablesById(Long tId)
    {
        return autoUserTablesMapper.selectAutoUserTablesById(tId);
    }

    /**
     * 查询用户建等级列表
     *
     * @param autoUserTables 用户建等级
     * @return 用户建等级
     */
    @Override
    public List<AutoUserTables> selectAutoUserTablesList(AutoUserTables autoUserTables)
    {
        return autoUserTablesMapper.selectAutoUserTablesList(autoUserTables);
    }

    /**
     * 新增用户建等级
     *
     * @param autoUserTables 用户建等级
     * @return 结果
     */
    @Override
    public int insertAutoUserTables(AutoUserTables autoUserTables)
    {
        return autoUserTablesMapper.insertAutoUserTables(autoUserTables);
    }

    /**
     * 修改用户建等级
     *
     * @param autoUserTables 用户建等级
     * @return 结果
     */
    @Override
    public int updateAutoUserTables(AutoUserTables autoUserTables)
    {
        return autoUserTablesMapper.updateAutoUserTables(autoUserTables);
    }

    /**
     * 批量删除用户建等级
     *
     * @param tIds 需要删除的用户建等级ID
     * @return 结果
     */
    @Override
    public int deleteAutoUserTablesByIds(Long[] tIds)
    {
        return autoUserTablesMapper.deleteAutoUserTablesByIds(tIds);
    }

    /**
     * 删除用户建等级信息
     *
     * @param tId 用户建等级ID
     * @return 结果
     */
    @Override
    public int deleteAutoUserTablesById(Long tId)
    {
        return autoUserTablesMapper.deleteAutoUserTablesById(tId);
    }
}
