package com.yabushan.form.domain;


public class FiledInsertInfo {

    private String tableName;
    private String filedName;
    private String filedValue;
    private Integer keyId;
    private  String filedId;


    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getFiledName() {
        return filedName;
    }

    public void setFiledName(String filedName) {
        this.filedName = filedName;
    }

    public String getFiledValue() {
        return filedValue;
    }

    public void setFiledValue(String filedValue) {
        this.filedValue = filedValue;
    }

    public Integer getKeyId() {
        return keyId;
    }

    public void setKeyId(Integer keyId) {
        this.keyId = keyId;
    }

    public String getFiledId() {
        return filedId;
    }

    public void setFiledId(String filedId) {
        this.filedId = filedId;
    }
}
