package com.yabushan.form.controller;

import java.util.List;

import com.yabushan.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.form.domain.AutoUserTables;
import com.yabushan.form.service.IAutoUserTablesService;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.common.core.page.TableDataInfo;

/**
 * 用户建等级Controller
 *
 * @author yabushan
 * @date 2021-08-06
 */
@RestController
@RequestMapping("/form/autotables")
public class AutoUserTablesController extends BaseController
{
    @Autowired
    private IAutoUserTablesService autoUserTablesService;

    /**
     * 查询用户建等级列表
     */
    @PreAuthorize("@ss.hasPermi('form:autotables:list')")
    @GetMapping("/list")
    public TableDataInfo list(AutoUserTables autoUserTables)
    {
        startPage();
        String username = SecurityUtils.getUsername();
        if(!username.equals("admin")){
            autoUserTables.setCreatedBy(username);
        }
        List<AutoUserTables> list = autoUserTablesService.selectAutoUserTablesList(autoUserTables);
        return getDataTable(list);
    }

    /**
     * 导出用户建等级列表
     */
    @PreAuthorize("@ss.hasPermi('form:autotables:export')")
    @Log(title = "用户建等级", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(AutoUserTables autoUserTables)
    {
        List<AutoUserTables> list = autoUserTablesService.selectAutoUserTablesList(autoUserTables);
        ExcelUtil<AutoUserTables> util = new ExcelUtil<AutoUserTables>(AutoUserTables.class);
        return util.exportExcel(list, "autotables");
    }

    /**
     * 获取用户建等级详细信息
     */
    @PreAuthorize("@ss.hasPermi('form:autotables:query')")
    @GetMapping(value = "/{tId}")
    public AjaxResult getInfo(@PathVariable("tId") Long tId)
    {
        return AjaxResult.success(autoUserTablesService.selectAutoUserTablesById(tId));
    }

    /**
     * 新增用户建等级
     */
    @PreAuthorize("@ss.hasPermi('form:autotables:add')")
    @Log(title = "用户建等级", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AutoUserTables autoUserTables)
    {
        return toAjax(autoUserTablesService.insertAutoUserTables(autoUserTables));
    }

    /**
     * 修改用户建等级
     */
    @PreAuthorize("@ss.hasPermi('form:autotables:edit')")
    @Log(title = "用户建等级", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AutoUserTables autoUserTables)
    {
        return toAjax(autoUserTablesService.updateAutoUserTables(autoUserTables));
    }

    /**
     * 删除用户建等级
     */
    @PreAuthorize("@ss.hasPermi('form:autotables:remove')")
    @Log(title = "用户建等级", businessType = BusinessType.DELETE)
	@DeleteMapping("/{tIds}")
    public AjaxResult remove(@PathVariable Long[] tIds)
    {
        return toAjax(autoUserTablesService.deleteAutoUserTablesByIds(tIds));
    }
}
