package com.yabushan.system.domain;

import com.yabushan.common.annotation.Excel;
import com.yabushan.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 数据服务API对象 data_api_infos
 *
 * @author yabushan
 * @date 2021-03-28
 */
public class DataApiInfos extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 资源ID */
    @Excel(name = "资源ID")
    private String resourceId;

    /** 资源编码 */
    @Excel(name = "资源编码")
    private String resourceCode;

    /** 资源名称 */
    @Excel(name = "资源名称")
    private String resourceName;

    /** 数据源ID */
    @Excel(name = "数据源ID")
    private String datasourceId;

    /** sql表达式 */
    @Excel(name = "sql表达式")
    private String apiDefination;

    /** 数据编码 */
    @Excel(name = "数据编码")
    private String apiCode;
    @Excel(name = "数据源名称")
    private String datasourceName;
    /** api状态 */
    @Excel(name = "api状态")
    private String apiStatus;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setResourceId(String resourceId)
    {
        this.resourceId = resourceId;
    }

    public String getResourceId()
    {
        return resourceId;
    }
    public void setResourceCode(String resourceCode)
    {
        this.resourceCode = resourceCode;
    }

    public String getResourceCode()
    {
        return resourceCode;
    }
    public void setResourceName(String resourceName)
    {
        this.resourceName = resourceName;
    }

    public String getResourceName()
    {
        return resourceName;
    }
    public void setDatasourceId(String datasourceId)
    {
        this.datasourceId = datasourceId;
    }

    public String getDatasourceId()
    {
        return datasourceId;
    }
    public void setApiDefination(String apiDefination)
    {
        this.apiDefination = apiDefination;
    }

    public String getApiDefination()
    {
        return apiDefination;
    }
    public void setApiCode(String apiCode)
    {
        this.apiCode = apiCode;
    }

    public String getApiCode()
    {
        return apiCode;
    }

    public void setApiStatus(String apiStatus)
    {
        this.apiStatus = apiStatus;
    }

    public String getApiStatus()
    {
        return apiStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("resourceId", getResourceId())
                .append("resourceCode", getResourceCode())
                .append("resourceName", getResourceName())
                .append("datasourceId", getDatasourceId())
                .append("apiDefination", getApiDefination())
                .append("apiCode", getApiCode())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .append("apiStatus", getApiStatus())
                .toString();
    }

    public String getDatasourceName() {
        return datasourceName;
    }

    public void setDatasourceName(String datasourceName) {
        this.datasourceName = datasourceName;
    }
}
