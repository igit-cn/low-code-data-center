package com.yabushan.activiti.tasklistener;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

public class SingleTaskListener  implements TaskListener{

	@Override
	public void notify(DelegateTask delegateTask) {

		String userName = (String) delegateTask.getVariable("requestUser");
		delegateTask.setAssignee(userName);
	}

}
